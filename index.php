<?
	function checkExp($exp)
	{
		$aStack = [];
		$aExpr = str_split($exp);
		for ($i = 0; count($aExpr); ++$i)
		{
			if (isOpenTag($aExpr[$i]))
			    array_push($aStack, $aExpr[$i]);
			elseif (isCloseTag($aExpr[$i]))
			{
				if (!$aStack || $aStack[$i] != getOpenTag($aExpr[$i])) return false;
				else
					array_pop($aStack);
			}
		}

		return $aStack? true : false;
	}

	function isOpenTag($ch)
	{
		switch ($ch)
		{
			case '(':
			case '{':
			case '[':
				return true;

			default:
				return false;
		}
	}

	function isCloseTag($ch)
	{
		switch ($ch)
		{
			case ')':
			case '}':
			case ']':
				return true;

			default:
				return false;
		}
	}

	function getOpenTag($ch)
	{
		switch ($ch)
		{
			case ')':
				return '(';

			case '}':
				return '{';

			case ']':
				return '[';

			default:
				return 0;
		}
	}

	if($_POST['str'])
    {
        $_POST['result'] = checkExp($_POST['str'])? 'Верно' : 'Неверно';
	}

?>


<div>
    <h2>Проверяет корректность баланса скобок</h2>
    <form method="post" action="">
        <input type="text" value="<?= $_POST['str']?>" name="str" placeholder="строка">
        <input type="submit" value="проверить">
        <div><?= $_POST['result']?></div>
    </form>
</div>