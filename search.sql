CREATE TABLE `test` (
	`id` INT(10) UNSIGNED NOT NULL,
	`text` TEXT NOT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

select id from test
group by id
having count(id) > 1;
